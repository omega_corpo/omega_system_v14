# -*- coding: utf-8 -*-

from . import employee_loan_type
from . import hr_employee
from . import employee_loan
from . import installment_line
from . import hr_payslip
from . import skip_installment
from . import loan_document
from . import employee_loan_report
