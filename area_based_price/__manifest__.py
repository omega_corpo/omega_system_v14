# noinspection PyStatementEffect
{
    'name': "Area Based Price",
    'version': '14.0.1.0.0',
    'author': 'Omega System',
    'maintainer': 'Omega System',
    'website': 'https://omegasystem.in/',
    'license': 'AGPL-3',
    'summary': """
        Calculates product prices based on their area""",
    'category': 'Sales',
    'depends': ['E3K_so_po_url', 'sale', 'account'],
    'data': [
    	'views/product_views.xml',
        'views/sale_order_view.xml',
        'views/purchase_order_view.xml',
        'views/account_invoice_views.xml',
        'views/res_company_view.xml',
        'report/report_sale.xml',
        'report/report_purchase.xml',
        'report/report_invoice.xml',
    ],
    'images': [
        'static/demo.gif',
        'static/description/banner.png',
    ],
    'price': 25.00,
    'currency': 'EUR',
    'installable': True,

}
