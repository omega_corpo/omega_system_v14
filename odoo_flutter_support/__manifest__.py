# -*- coding: utf-8 -*-
{
    'name': 'OS Odoo Flutter Support',
    'version': '14.0.1.0.0',
    'author': 'Omega System',
    'maintainer': 'Omega System',
    'website': 'https://omegasystem.in/',
    'license': 'AGPL-3',
    'category': 'Web',
    'summary': 'Quick Support of Odoo Flutter Mobile Application',
    'depends': [
        'stock'
    ],
    'data': [
        'views/stock_picking.xml',
    ],
    'images': ['static/description/banner.png'],
    'installable': True,
    'application': True,
}
