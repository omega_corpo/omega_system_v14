# -*- coding: utf-8 -*-
from odoo import fields, models


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    partner_name = fields.Char(related='partner_id.name',
                               string='Partner Name')
    partner_external_id = fields.Integer(related='partner_id.id', string='Partner ID')
    picking_external_type_id = fields.Integer(related='picking_type_id.id', string='Picking Type ID')

    def validate_picking(self):
        receipt = self.env['stock.picking'].browse(self.id)
        receipt.action_confirm()
        transfer = self.env['stock.immediate.transfer'].create({'pick_ids': [(4, receipt.id)]})
        self.env['stock.immediate.transfer.line'].create({
            'picking_id': receipt.id,
            'immediate_transfer_id': transfer.id,
            'to_immediate': True
        })
        transfer.with_context(button_validate_picking_ids=receipt.id).process()
