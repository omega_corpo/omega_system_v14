# -*- coding: utf-8 -*-
from odoo import api, fields, models


class IrModel(models.Model):
    _inherit = 'ir.model'

    # def get_selection_fields(self):
    #     print("self------------", self._origin.id)
    #     classname = ['default', 'primary', 'success', 'warning', 'danger']
    #     return [(x, x) for x in classname]

    show_in_portal = fields.Boolean('Show In Portal')
    sort_by = fields.Selection([
        ('new', 'Newest'),
        ('old', 'Oldest'),
        ('name', 'Name')
    ], string='Sort By')
    filter_by = fields.Selection([
        ('today', 'Today'),
        ('this_month', 'This Month'),
        ('this_week', 'This Week'),
        ('this_year', 'This Year'),
        ('last_month', 'Last Month'),
        ('last_week', 'Last Week'),
        ('last_year', 'Last Year')
    ], string='Filter By')
    fitler_by_id = fields.Many2one('ir.model.fields', domain="[('model_id', '=', model), ('ttype', '=', 'many2one')]")
    advance_fields_ids = fields.One2many('advance.portal.fields', 'model_id', string='Advance Fields')

    def generate_advance_fields(self):
        fields_ids = self.env['ir.model.fields'].search([
            ('model_id', '=', self.id)
        ])
        field_values = [(0, 0, {'field_id': line.id, 'model_id': self.id}) for line in fields_ids]
        self.update({'advance_fields_ids': field_values})


class AdvancePortalFields(models.Model):
    _name = 'advance.portal.fields'

    model_id = fields.Many2one('ir.model', string='Model')
    field_id = fields.Many2one('ir.model.fields', string='Field')
    show_in_portal = fields.Boolean('Show In Portal')
    # is_filter_by = fields.Boolean('Is Filter By')
    is_sort_by = fields.Boolean('Is Sort By')
    sequence = fields.Integer('Sequence')


class IrModelFields(models.Model):
    _inherit = 'ir.model.fields'

    show_in_portal = fields.Boolean('Show In Portal')
    is_group_by = fields.Boolean('Is Group By')
