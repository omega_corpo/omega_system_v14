# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from collections import OrderedDict
from dateutil.relativedelta import relativedelta
from operator import itemgetter
from datetime import datetime

from odoo import fields, http, _
from odoo.http import request
from odoo.tools import date_utils, groupby as groupbyelem
from odoo.osv.expression import AND, OR

from odoo.addons.portal.controllers.portal import CustomerPortal, pager as portal_pager


class AdvancePortal(CustomerPortal):

    def _prepare_home_portal_values(self, counters):

        values = super()._prepare_home_portal_values(counters)
        # if 'timesheet_count' in counters:
        #     domain = request.env['account.analytic.line']._timesheet_get_portal_domain()
        values['timesheet_count'] = 10
        values['title'] = 'Test'
        values['list_of_name'] = ['abc', 'fsdf']
        print('ca;--------------------------', values)
        return values

    # def _get_searchbar_inputs(self):
    #     return {
    #         'all': {'input': 'all', 'label': _('Search in All')},
    #         'project': {'input': 'project', 'label': _('Search in Project')},
    #         'name': {'input': 'name', 'label': _('Search in Description')},
    #         'employee': {'input': 'employee', 'label': _('Search in Employee')},
    #         'task': {'input': 'task', 'label': _('Search in Task')}
    #     }
    #
    # def _get_searchbar_groupby(self):
    #     return {
    #         'none': {'input': 'none', 'label': _('None')},
    #         'project': {'input': 'project', 'label': _('Project')},
    #         'task': {'input': 'task', 'label': _('Task')},
    #         'date': {'input': 'date', 'label': _('Date')},
    #         'employee': {'input': 'employee', 'label': _('Employee')}
    #     }
    #
    # def _get_search_domain(self, search_in, search):
    #     search_domain = []
    #     if search_in in ('project', 'all'):
    #         search_domain = OR([search_domain, [('project_id', 'ilike', search)]])
    #     if search_in in ('name', 'all'):
    #         search_domain = OR([search_domain, [('name', 'ilike', search)]])
    #     if search_in in ('employee', 'all'):
    #         search_domain = OR([search_domain, [('employee_id', 'ilike', search)]])
    #     if search_in in ('task', 'all'):
    #         search_domain = OR([search_domain, [('task_id', 'ilike', search)]])
    #     return search_domain
    #
    # def _get_groupby_mapping(self):
    #     return {
    #         'project': 'project_id',
    #         'task': 'task_id',
    #         'employee': 'employee_id',
    #         'date': 'date'
    #     }
    #

    # def call_controller(self, type='http', auth="user", website=True):


    @http.route(['/my/advance_portal'], type='http', auth="user", website=True)
    def advance_portal(self, page=1, sortby=None, filterby=None, search=None, search_in='all', groupby='none', **kw):
        values = self._prepare_portal_layout_values()
        model_ids = request.env['ir.model'].search([
            ('show_in_portal', '=', True)
        ])

        portal_list = []
        for model in model_ids:
            portal_list.append({'model': model})
        values.update({
            'title': 'Portal',
            'portal_list': portal_list
        })
        print("values-------", values)
        return request.render("os_advance_portal.portal_advance", values)

    @http.route(['/my/advance_portal/table/<int:model_id>'], type='http', auth="public", website=True)
    def advance_portal_table(self, model_id=None, page=1, sortby=None, filterby=None, search=None, search_in='content', groupby=None, **kw):
        print("sortby---------", sortby, filterby, search, groupby)
        fields_list = []
        searchbar_sortings = {
            'date': {'label': _('Newest'), 'order': 'create_date desc'},
            'name': {'label': _('Title'), 'order': 'name'},
        }

        searchbar_filters = {
            'all': {'label': _('All'), 'domain': []},
        }

        values = self._prepare_portal_layout_values()

        count_fields_ids = request.env['advance.portal.fields'].search_count([
            ('model_id', '=', model_id),
            ('show_in_portal', '=', True)
        ])
        model_id = request.env['ir.model'].browse(model_id)
        model_data = request.env[model_id.model].search([])

        fields = []
        pager = portal_pager(
            url="/my/advance_portal/table/79",
            url_args={'sortby': sortby, 'filterby': filterby,
                      'groupby': groupby, 'search_in': search_in, 'search': search},
            total=count_fields_ids,
            page=page,
            step=self._items_per_page
        )
        fields_ids = request.env['advance.portal.fields'].search([
            ('model_id', '=', model_id.id),
            ('show_in_portal', '=', True)
        ])
        sort_by_ids = [group_fields for group_fields in fields_ids.filtered(lambda x: x.is_sort_by is True)]
        # filter_by_ids = [filter_fields for filter_fields in fields_ids.filtered(lambda x: x.is_filter_by is True)]
        # print("filter_by_ids----------------", filter_by_ids)
        if not filterby:
            filterby = 'all'

        # SORT BY
        for sort_id in sort_by_ids:
            searchbar_sortings.update({
                sort_id.field_id.name : {'order': sort_id.field_id.name, 'label': _(sort_id.field_id.name)}
            })

        fields_names = []
        for field_id in fields_ids:
            fields_name_dict = {}
            fields_list.append({'fields': field_id})
            fields.append(field_id.field_id.name)
            fields_name_dict[field_id.field_id.name] = field_id.field_id.field_description
            fields_names.append(fields_name_dict)
        if not sortby:
            sortby = 'name'
        if model_id.fitler_by_id:
            filter_datas = request.env[model_id.fitler_by_id.relation].search([])
            for filter_data in filter_datas:
                searchbar_filters.update({
                    filter_data.name: {'label': _(filter_data.name), 'domain': []},
                })
        order = searchbar_sortings[sortby]['order']
        domain = []

        if filterby != "all":
            domain += [(model_id.fitler_by_id.name, '=', filterby)]
        else:
            filterby = "all"
        all_data = request.env[model_id.model].search_read(
            domain, fields, order=order, limit=self._items_per_page, offset=pager['offset'])
        print("all_data------------", all_data[0].keys(), fields_names)
        for data in all_data:
            # if data.keys() in fields_names[0]:
            print("data--------",data.keys())

        sortby = 'date'
        values.update({
            'fields_list': fields_list,
            'model_datas': model_data,
            'all_data': all_data,
            'fields_len': len(fields_ids),
            'sortby': sortby,
        })
        values.update({
            'searchbar_sortings': searchbar_sortings,
            'sortby': sortby,
            'default_url': '/my/advance_portal/table/79',
            'search_in': search_in,
            'search': search,
            'pager': pager,
            'searchbar_filters': OrderedDict(sorted(searchbar_filters.items())),
            'filterby': filterby,
        })
        return request.render("os_advance_portal.portal_advance_table", values)
