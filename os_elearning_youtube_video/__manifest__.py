# -*- coding: utf-8 -*-

{
    'name': 'e-learning private youtube video',
    'version': '14.0.1.0.0',
    'author': 'Omega System',
    'maintainer': 'Omega System',
    'website': 'https://omegasystem.in/',
    'license': 'AGPL-3',
    'category': 'Website',
    'summary': 'OS e-learning private youtube video',
    'depends': ['base', 'website_slides'],
    'data': [
        'views/assets.xml',
    ],
    'images': ['static/description/banner.png'],
    'price': 15.00,
    'currency': 'EUR',
    'installable': True,
    'application': True,
}
