odoo.define('os_elearning_youtube_video.fullscreen_youtube_video', function (require) {
"use strict";

    var core = require('web.core');
    var QWeb = core.qweb;
    var Video = require('website_slides.fullscreen');
    var Fullscreen = require('website_slides.fullscreen');

    Fullscreen.include({
        xmlDependencies: (Fullscreen.prototype.xmlDependencies || []).concat(
            ["/os_elearning_youtube_video/static/xml/website.xml"]
        ),
        init: function (parent, slides, defaultSlideId, channelData){
            var result = this._super.apply(this,arguments);
            return result;
        },

    });

});