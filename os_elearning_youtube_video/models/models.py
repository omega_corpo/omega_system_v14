# -*- coding: utf-8 -*-

from odoo import api, models, _
from odoo.http import request
from odoo.addons.http_routing.models.ir_http import url_for


class SlideSlideInherited(models.Model):
    _inherit = 'slide.slide'

    @api.depends('document_id', 'slide_type', 'mime_type')
    def _compute_embed_code(self):
        base_url = request and request.httprequest.url_root or self.env['ir.config_parameter'].sudo().get_param(
            'web.base.url')
        if base_url[-1] == '/':
            base_url = base_url[:-1]
        for record in self:
            if record.datas and (not record.document_id or record.slide_type in ['document', 'presentation']):
                slide_url = base_url + url_for('/slides/embed/%s?page=1' % record.id)
                record.embed_code = '<iframe sandbox="allow-forms allow-scripts allow-pointer-lock allow-same-origin allow-top-navigation" class="o_wslides_iframe_viewer" src="%s" allowFullScreen="true" height="%s" width="%s" frameborder="0"></iframe>' % (
                slide_url, 315, 420)
            elif record.slide_type == 'video' and record.document_id:
                if not record.mime_type:
                    record.embed_code = '<iframe sandbox="allow-forms allow-scripts allow-pointer-lock allow-same-origin allow-top-navigation"  src="//www.youtube.com/embed/%s?rel=0&theme=light&showinfo=0&autoplay=1"  frameborder="0"></iframe><div   class="youtube-overlay"></div><div class="youtube-overlay3"/></div>' % (
                        record.document_id)
                else:
                    record.embed_code = '<iframe sandbox="allow-forms allow-scripts allow-pointer-lock allow-same-origin allow-top-navigation"  src="//drive.google.com/file/d/%s/preview" frameborder="0"></iframe><div class="youtube-overlay"></div><div class="youtube-overlay3"></div>' % (
                        record.document_id)
            else:
                record.embed_code = False
