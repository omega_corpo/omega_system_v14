# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from collections import OrderedDict
from dateutil.relativedelta import relativedelta
from operator import itemgetter
from datetime import datetime

from odoo import fields, http, _
from odoo.http import request
from odoo.tools import date_utils, groupby as groupbyelem
from odoo.osv.expression import AND, OR

from odoo.addons.portal.controllers.portal import CustomerPortal, pager as portal_pager


class AdvancePortal(CustomerPortal):

    def _prepare_home_portal_values(self, counters):

        values = super()._prepare_home_portal_values(counters)
        # if 'timesheet_count' in counters:
        #     domain = request.env['account.analytic.line']._timesheet_get_portal_domain()
        values['timesheet_count'] = 10
        values['title'] = 'Test'
        values['list_of_name'] = ['abc', 'fsdf']
        print('ca;--------------------------', values)
        return values

    # def _get_searchbar_inputs(self):
    #     return {
    #         'all': {'input': 'all', 'label': _('Search in All')},
    #         'project': {'input': 'project', 'label': _('Search in Project')},
    #         'name': {'input': 'name', 'label': _('Search in Description')},
    #         'employee': {'input': 'employee', 'label': _('Search in Employee')},
    #         'task': {'input': 'task', 'label': _('Search in Task')}
    #     }
    #
    # def _get_searchbar_groupby(self):
    #     return {
    #         'none': {'input': 'none', 'label': _('None')},
    #         'project': {'input': 'project', 'label': _('Project')},
    #         'task': {'input': 'task', 'label': _('Task')},
    #         'date': {'input': 'date', 'label': _('Date')},
    #         'employee': {'input': 'employee', 'label': _('Employee')}
    #     }
    #
    # def _get_search_domain(self, search_in, search):
    #     search_domain = []
    #     if search_in in ('project', 'all'):
    #         search_domain = OR([search_domain, [('project_id', 'ilike', search)]])
    #     if search_in in ('name', 'all'):
    #         search_domain = OR([search_domain, [('name', 'ilike', search)]])
    #     if search_in in ('employee', 'all'):
    #         search_domain = OR([search_domain, [('employee_id', 'ilike', search)]])
    #     if search_in in ('task', 'all'):
    #         search_domain = OR([search_domain, [('task_id', 'ilike', search)]])
    #     return search_domain
    #
    # def _get_groupby_mapping(self):
    #     return {
    #         'project': 'project_id',
    #         'task': 'task_id',
    #         'employee': 'employee_id',
    #         'date': 'date'
    #     }
    #

    # def call_controller(self, type='http', auth="user", website=True):


    @http.route(['/my/advance_portal'], type='http', auth="user", website=True)
    def advance_portal(self, page=1, sortby=None, filterby=None, search=None, search_in='all', groupby='none', **kw):
        values = self._prepare_portal_layout_values()
        model_ids = request.env['ir.model'].search([
            ('show_in_portal', '=', True)
        ])

        portal_list = []
        for model in model_ids:
            portal_list.append({'model': model})
        values.update({
            'title': 'Portal',
            'portal_list': portal_list
        })
        print("values-------", values)
        return request.render("os_advance_portal.portal_advance", values)

    @http.route(['/my/advance_portal/table/<int:model_id>'], type='http', auth="public", website=True)
    def advance_portal_table(self, model_id=None, page=1, sortby=None, filterby=None, search=None, search_in='content', groupby=None, **kw):
        print("sortby---------", sortby, filterby, search, groupby)
        fields_list = []
        searchbar_sortings = {
            'date': {'label': _('Newest'), 'order': 'create_date desc'},
            'name': {'label': _('Title'), 'order': 'name'},
        }

        searchbar_groupby = {
            'none': {'input': 'none', 'label': _('None')},
            'name': {'label': _('Title'), 'order': 'name'},
        }

        values = self._prepare_portal_layout_values()

        count_fields_ids = request.env['advance.portal.fields'].search_count([
            ('model_id', '=', model_id),
            ('show_in_portal', '=', True)
        ])
        model_id = request.env['ir.model'].browse(model_id)
        model_data = request.env[model_id.model].search([])

        fields = []
        pager = portal_pager(
            url="/my/advance_portal/table/79",
            url_args={'sortby': sortby, 'filterby': filterby,
                      'groupby': groupby, 'search_in': search_in, 'search': search},
            total=count_fields_ids,
            page=page,
            step=self._items_per_page
        )
        fields_ids = request.env['advance.portal.fields'].search([
            ('model_id', '=', model_id.id),
            ('show_in_portal', '=', True)
        ])
        group_by_ids = [group_fields for group_fields in fields_ids.filtered(lambda x: x.is_group_by is True)]
        sort_by_ids = [group_fields for group_fields in fields_ids.filtered(lambda x: x.is_sort_by is True)]

        # GROUP BY
        for group_id in group_by_ids:
            searchbar_groupby.update({
                group_id.field_id.name : {'input': group_id.field_id.name, 'label': _(group_id.field_id.name)}
            })
        print("searchbar_groupby----------", searchbar_groupby)
        if not groupby:
            groupby = 'name'


        # SORT BY
        for sort_id in sort_by_ids:
            searchbar_sortings.update({
                sort_id.field_id.name : {'order': sort_id.field_id.name, 'label': _(sort_id.field_id.name)}
            })

        for field_id in fields_ids:
            fields_list.append({'fields': field_id})
            fields.append(field_id.field_id.name)
        if not sortby:
            sortby = 'name'
        order = searchbar_sortings[sortby]['order']

        all_data = request.env[model_id.model].search_read(
            [], fields, order=order, limit=self._items_per_page, offset=pager['offset'])

        grouped_datas = [request.env[model_id.model].concat(*g) for k, g in
                         groupbyelem(model_data, itemgetter('country_id'))]
        all_grouped_data = []
        for grouped_data in grouped_datas:
            for id in grouped_data:
                # print("grouped_data------------", id)
                all_grouped_data.append(id)
        sortby = 'date'
        values.update({
            'fields_list': fields_list,
            'model_datas': model_data,
            'all_data': all_data,
            'fields_len': len(fields_ids),
            'sortby': sortby,
        })
        values.update({
            # 'date': date_begin,
            # 'page_name': 'order',
            # 'pager': pager,
            # 'default_url': '/my/orders',
            'grouped_data': grouped_datas,
            'searchbar_sortings': searchbar_sortings,
            'sortby': sortby,
            'default_url': '/my/advance_portal/table/79',
            'searchbar_groupby': searchbar_groupby,
            'groupby': groupby,
            'search_in': search_in,
            'search': search,
            'pager': pager,
        })
        # print("model_data-------", values)
        return request.render("os_advance_portal.portal_advance_table", values)
