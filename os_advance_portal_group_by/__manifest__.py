# -*- coding: utf-8 -*-
{
    'name': 'OS Advance Portal',
    'version': '14.0.1.0.0',
    'author': 'Omega System',
    'maintainer': 'Omega System',
    'website': 'https://omegasystem.in/',
    'license': 'AGPL-3',
    'category': 'Web',
    'summary': 'Quick Design Odoo Portal',
    'depends': [
        'web',
        'base',
        'portal'
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/advance_portal_view.xml',
        'views/advance_portal.xml'
        # 'views/ir_model.xml',
    ],
    'images': ['static/description/banner.png'],
    'price': 25.00,
    'currency': 'EUR',
    'installable': True,
    'application': True,
}
