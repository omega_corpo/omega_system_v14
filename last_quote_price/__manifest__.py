# -*- coding: utf-8 -*-

{
    'name': 'Last Quote and Purchase Price',
    'version': '14.0.1.0.0',
    'category': 'Tools',
    'summary': """ Last Quote and Purchase Price """,
    'description': """""",
    'author': ' Omega System',
    'company': 'Omega System',
    'maintainer': 'Omega System',
    'website': "https://omegasystem.in/",
    'depends': ['sale'],
    'data': [
    ],
    'license': 'LGPL-3',
    'price': 15.00,
    'currency': 'EUR',
    'installable': True,
    'auto_install': False,
    'application': False,
}

