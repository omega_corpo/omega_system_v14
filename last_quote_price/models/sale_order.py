# -*- coding: utf-8 -*-
from odoo import api, models, http, fields, _
import base64
import uuid
from odoo.tools.misc import formatLang, get_lang

from odoo.exceptions import UserError, ValidationError


class SaleOrder(models.Model):
    _inherit = "sale.order"

    def action_confirm(self):
        res = super(SaleOrder, self).action_confirm()
        for lines in self.order_line:
            print("line--------", lines)
            lines.product_id.last_sale_price = lines.price_unit

        return res

class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    @api.onchange('product_uom', 'product_uom_qty')
    def product_uom_change(self):
        if not self.product_uom or not self.product_id:
            self.price_unit = 0.0
            return
        if self.order_id.pricelist_id and self.order_id.partner_id:
            product = self.product_id.with_context(
                lang=self.order_id.partner_id.lang,
                partner=self.order_id.partner_id,
                quantity=self.product_uom_qty,
                date=self.order_id.date_order,
                pricelist=self.order_id.pricelist_id.id,
                uom=self.product_uom.id,
                fiscal_position=self.env.context.get('fiscal_position')
            )
            last_id = self.env['sale.order.line'].search([
                ('product_id', '=', self.product_id.id),
                ('order_id.state', '=', 'sale')
            ])
            price = last_id.product_id.last_sale_price if last_id.product_id.last_sale_price else self.product_id.lst_price
            self.price_unit = self.env['account.tax']._fix_tax_included_price_company(price,
                                                                                      product.taxes_id, self.tax_id,
                                                                                      self.company_id)

    @api.onchange('product_id')
    def product_id_change(self):
        if not self.product_id:
            return
        last_id = self.env['sale.order.line'].search([
            ('product_id', '=', self.product_id.id),
            ('order_id.state', '=', 'sale')
        ])
        valid_values = self.product_id.product_tmpl_id.valid_product_template_attribute_line_ids.product_template_value_ids
        # remove the is_custom values that don't belong to this template
        for pacv in self.product_custom_attribute_value_ids:
            if pacv.custom_product_template_attribute_value_id not in valid_values:
                self.product_custom_attribute_value_ids -= pacv

        # remove the no_variant attributes that don't belong to this template
        for ptav in self.product_no_variant_attribute_value_ids:
            if ptav._origin not in valid_values:
                self.product_no_variant_attribute_value_ids -= ptav

        vals = {}
        if not self.product_uom or (self.product_id.uom_id.id != self.product_uom.id):
            vals['product_uom'] = self.product_id.uom_id
            vals['product_uom_qty'] = self.product_uom_qty or 1.0

        product = self.product_id.with_context(
            lang=get_lang(self.env, self.order_id.partner_id.lang).code,
            partner=self.order_id.partner_id,
            quantity=vals.get('product_uom_qty') or self.product_uom_qty,
            date=self.order_id.date_order,
            pricelist=self.order_id.pricelist_id.id,
            uom=self.product_uom.id
        )

        vals.update(name=self.get_sale_order_line_multiline_description_sale(product))

        self._compute_tax_id()
        vals['price_unit'] = last_id.product_id.last_sale_price if last_id.product_id.last_sale_price else self.product_id.lst_price
        if self.order_id.pricelist_id and self.order_id.partner_id:
            vals['price_unit'] = last_id.product_id.last_sale_price if last_id.product_id.last_sale_price else  self.product_id.lst_price
        self.update(vals)

        title = False
        message = False
        result = {}
        warning = {}
        if product.sale_line_warn != 'no-message':
            title = _("Warning for %s", product.name)
            message = product.sale_line_warn_msg
            warning['title'] = title
            warning['message'] = message
            result = {'warning': warning}
            if product.sale_line_warn == 'block':
                self.product_id = False

        return result


class ProductTemplate(models.Model):
    _inherit = "product.template"

    last_sale_price = fields.Float('Last Sale Price')
    last_purchase_price = fields.Float('Last Purchase Price')
