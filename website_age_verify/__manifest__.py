# -*- coding: utf-8 -*-
{
    'name': 'Website Popup Age Verify',
    'version': '14.0.1.0.0',
    'author': 'Omega System',
    'maintainer': 'Omega System',
    'website': 'https://omegasystem.in/',
    'license': 'AGPL-3',
    'category': 'Web',
    'summary': 'Website Popup for Age Verify',
    'depends': [
        'website_sale'
    ],
    'data': [
        # 'views/assets.xml',
        'views/res_config_view.xml'
    ],
    'assets': {
        'web.assets_frontend': [
            "/website_age_verify/static/src/css/age-verification-public.css",
            "/website_age_verify/static/src/js/age-verification.js",
        ]
    },
    'images': ['static/description/banner.png'],
    'price': 15.00,
    'currency': 'EUR',
    'installable': True,
    'application': True,
}
