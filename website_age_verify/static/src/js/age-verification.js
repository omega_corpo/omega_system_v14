odoo.define('website_age_verify.age_verify', function (require) {
"use strict";

    require('web.dom_ready');
    var publicWidget = require('web.public.widget');
    var ajax = require('web.ajax');
    var rpc = require('web.rpc');
    var utils = require('web.utils');
    var tour = require('web_tour.tour');
    var core = require('web.core');

    var modal_content,
    modal_screen;
    var QWeb = core.qweb;
    var _t = core._t;

    $(document).ready(function() {
        av_legality_check();
    });

    function av_legality_check() {
        if (utils.get_cookie('is_legal') == "yes") {
            // legal!
            // Do nothing?
        } else {
            av_showmodal();
            // Make sure the prompt stays in the middle.
            $(window).on('resize', av_positionPrompt);
        }
    };
     function av_showmodal() {
        var get_age = 0;
        ajax.jsonRpc('/get_age', 'call', {}).then(function(data) {
            get_age = data
            if (get_age){
                modal_screen = $('<div id="" class="wpd-av-overlay"></div>');
                modal_content = $('<div class="wpd-av" style="display: flex; justify-content: center;align-items: center; text-align: center; min-height: 100vh;opacity: 1;"></div>');
                var modal_content_wrapper = $('<div id="modal_content_wrapper" class="content_wrapper"></div>');
                var modal_regret_wrapper = $('<div id="modal_regret_wrapper" class="content_wrapper" style="display:none;"></div>');

                var content_heading = $('<h2>Age Verification</h2><p>You must be <strong>'+get_age+'</strong> years old to enter.</p><p><button class="yes av_btn" rel="yes">YES</button><button class="no av_btn" rel="no">NO</button></p>');
                var regret_heading = $('<h2>Sorry!</h2>');
                var regret_text = $('<p>You are not old enough to view this site...</p>');

                modal_content_wrapper.append(content_heading);
                modal_regret_wrapper.append(regret_heading, regret_text);

                modal_content.append(modal_content_wrapper, modal_regret_wrapper);

                $('body').append(modal_screen, modal_content);

                av_positionPrompt();

                modal_content.find('button.av_btn').on('click', av_setCookie);
            }
        });

    };

     function av_setCookie(e) {
        e.preventDefault();
        var is_legal = $(e.currentTarget).attr('rel');
//        document.cookie('is_legal', is_legal, {
//            expires: 30,
//            path: '/'
//        });
        utils.set_cookie('is_legal', is_legal, 30);

        if (is_legal == "yes") {
            av_closeModal();
            $(window).off('resize');
        } else {
            av_showRegret();
        }
    };

    function av_closeModal() {
        modal_content.fadeOut();
        modal_screen.fadeOut();
    };

    function av_showRegret() {
        modal_screen.addClass('nope');
        modal_content.find('#modal_content_wrapper').hide();
        modal_content.find('#modal_regret_wrapper').show();
    };

    function av_positionPrompt() {
        var top = ($(window).outerHeight() - $('#modal_content').outerHeight()) / 2;
        var left = ($(window).outerWidth() - $('#modal_content').outerWidth()) / 2;
        modal_content.css({
            'top': top,
            'left': left
        });

        if (modal_content.is(':hidden') && (utils.get_cookie('is_legal') != "yes")) {
            modal_content.fadeIn('slow')
        }
    };

});


