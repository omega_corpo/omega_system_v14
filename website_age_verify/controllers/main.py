# -*- coding: utf-8 -*-

from odoo import http, _
from odoo.http import request
from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo.addons.website_sale.controllers import main


class WebsiteSale(WebsiteSale):

    @http.route('/get_age', type='json', auth='public', website=True)
    def get_age(self, **kw):
        get_param = request.env['ir.config_parameter'].sudo().get_param
        return get_param('website_age_verify.age')
