# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    url = fields.Char(
        string='Url'
    )
    oorder_line_id = fields.Many2one(
        'sale.order.line',
        string='OSale Order'
    )
