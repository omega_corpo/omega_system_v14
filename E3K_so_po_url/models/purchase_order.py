# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    url = fields.Char(
        string='Url'
    )
    sent_to = fields.Many2one('res.partner',
        string='Send To'
    )
