# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from collections import defaultdict
from dateutil.relativedelta import relativedelta
from itertools import groupby

from odoo import api, fields, models, _, SUPERUSER_ID
from odoo.exceptions import UserError


class StockRule(models.Model):
    _inherit = 'stock.rule'
    #
    # def _update_purchase_order_line(self, product_id, product_qty, product_uom, company_id, values, line):
    #     res = super(StockRule, self)._update_purchase_order_line(product_id, product_qty, product_uom, company_id, values, line)
    #     url = ''
    #     oorder_line_id = False
    #     if values.get('move_dest_ids'):
    #         line = values['move_dest_ids'].sale_line_id
    #         url = line.url
    #         oorder_line_id = line.id
    #
    #     res['url'] = line.url
    #     res['oorder_line_id'] = oorder_line_id
    #
    #     return res
    #
    #
    # @api.model
    # def _prepare_purchase_order_line(self, product_id, product_qty, product_uom, company_id, values, po):
    #     res = super(StockRule, self)._prepare_purchase_order_line(product_id, product_qty, product_uom, company_id, values, po)
    #     url = ''
    #     oorder_line_id = False
    #     if values.get('move_dest_ids'):
    #         line = values['move_dest_ids'].sale_line_id
    #         url = line.url
    #         oorder_line_id = line.id
    #
    #     res['url']  = line.url
    #     res['oorder_line_id']  = oorder_line_id
    #
    #     return res