# -*- coding: utf-8 -*-
{
    'name': "E3K so_po_url",
    'author': "E3K S.E.N.C.",
    'website': "https://www.e3k.ca/",
    'category': 'Sale',
    'summary': """ """,
    'License': 'LGPL Version 3',
    'version': '13.1.0.1',
    'depends': [
        'sale_management',
        'purchase'
    ],
    'data': [
        'views/sale_order_views.xml',
        'views/purchase_order_views.xml',
        
        # 'report/purchase_quotation_templates.xml',
        # 'report/purchase_order_templates.xml',
        # 'report/sale_order_template.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
