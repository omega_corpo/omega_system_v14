{
    'name': 'Speech To Text',
    'version': '1.0',
    'category': '',
    'summary': '',
    'author': 'Abhinav Karthik R',
    'website': '',
    'license': 'AGPL-3',
    'depends': ['base','web',],
    'data': [
         'views/speech_text_view.xml',
         'security/ir.model.access.csv',
    ],
    'installable': True,
}