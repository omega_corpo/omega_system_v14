from odoo import fields,models,api
import speech_recognition as sr


class SpeechRecognizer(models.Model):

    _name = "speech.text"

    textbox = fields.Text(string="Text")
    stop = fields.Integer()
    # i = 1
    # @api.multi
    def record(self):
        r = sr.Recognizer()
        with sr.Microphone() as source:
            print("Speak Anything :")
            audio = r.listen(source)
            self.stop = 0
            try:
                # textlist = []

                text = r.recognize_google(audio)
                self.textbox = text
                # if(self.i <= 10):

                    # textlist.append(text)
                    # self.i += 1
                    # self.record()
                # print(textlist)
                # self.textbox = '  '.join(map(str,textlist))
                print("You said : {}".format(text))
            except:
                print("Sorry could not recognize what you said")
    # @api.multi
    # def stoprec(self):
    #     self.stop = 1